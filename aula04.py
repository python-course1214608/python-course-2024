import os, math, random
import flask
import sqlite3


# Funções -> Trechos de códigos que podem ser definidos para serem usados e 
# reutilizados no decorrer do programa

idade = 10

# Função para limpar a tela
def limpa_tela():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

# Função para boas vindas
def boas_vinda():
    nome = input("Qual é o seu nome? ")
    print(f"Seja muito bem vindo {nome}!")
    print(idade)

boas_vindas()                        # Chamar a função

def soma():
    resultado = x + y 
    print(resultado)

limpa_tela()
boas_vindas()
soma(14, 25)

# -------------------------------------------------
def soma(x, y): 
        resultado = x + y
        return resultado

# resp = int(input("Digite um numero"))
# soma(resp, 25)

print(f"A soma de 14 e 29 é {soma(14, 29)}")

print(soma(40,39))
print(soma(y=40,x=30))

# -------------------------------------------------
def soma(*, x, y):      # o asterisco (*) é usado apenas para apontar o X e o Y
    resultado = x+ y    
    return resultado

print(soma(x=40,y=30))

# -------------------------------------------------
def soma(x, y,/):      
    resultado = x+ y    
    return resultado

print(soma(40,30))


# -------------------------------------------------
def mult(*x):
    return(x)
print(mult(10, 32, 2, 45, 44))

def mult(*x,y):
    return(x)    
print(mult(10, 32, 2, 45, 44,y=5))

def mult(*x,y):
    for valor in x:    
        print(f"{valor} x {y} = {valor * y}")
mult(10, 32, 2, 10, 12, y=5) 

print(mult(10, 32, 2, 45, 44,y=5))

# -------------------------------------------------
# Funções Lambdas / Funções Anônimas

def soma1(x,y):
    return(x + y)

soma2 = lambda x,y: x + y

print(soma1(10,15))
print(soma2(10,15))


# -------------------------------------------------
# Módulos

# Existem 3 maneiras de ter acesso a novos módulos no python
# Módulos NATIVOS e carregados (print e input)
# Módulos NATIVOS mas não importados por padrão (os, math, randon)
# Módulos não NATIVOS que podem ser baixados pelo gerenciador de pacotes PIP
#         - flask e pymongo
# Baixando módulos criados e disponibilizados pela comunidade




























