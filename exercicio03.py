# ======================================================================================================
# 1) Escrever um script de Python que substitua o caractere "X" por espaço considerando frase:
# "Umxpratoxdextrigoxparaxtresxtigresxtristes"
# ======================================================================================================

frase = "Umxpratoxdextrigoxparaxtresxtigresxtristes"
frase = frase.replace("x"," ")

print(frase)

# ======================================================================================================
# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z
# ======================================================================================================

nasc = int(input("Insira o ano em que você nasceu: "))

if nasc <= 1964:
     print("Geração Baby boomer")
elif nasc >= 1965 and nasc <= 1979:
     print("Geração X")
elif nasc >= 1980 and nasc <= 1994:
     print("Geração Y")
elif nasc >= 1995:
     print("Geração Z")



