import os
import math
# ====================================================
# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.
# O argumento da função deverá ser o nome, e saída deverá ser como a seguir:
# chamada da função: saudacao('Lalo')
# saída: 'Olá Lalo! Tudo bem com você?'

def saudacao():
     nome = input(f"Qual é o seu nome? ")
     print(f"Saudacao {nome}!")
     print(f"Ola {nome}! Tudo bem com você?")

saudacao()

# ====================================================
# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

def limpa_tela():
     if os.name == "nt": 
          os.system("cls")
     else:
          os.system("clear")     

def soma(a, b):
    return a + b

def diferenca(a, b):
    return a - b

def multiplicacao(a, b):
    return a * b

def divisao(a, b):
    if b != 0:
        return a / b
    else:
        return "Erro: divisão por zero"

def calculadora():
    num1 = float(input("Digite o primeiro número: "))
    num2 = float(input("Digite o segundo número: "))

    print(f"""
    Escolha a operação desejada
    1 - Soma
    2 - Diferença
    3 - Multiplicação
    4 - Divisão
    """)
        
    oper = input("Digite o numero da operação desejada ") 

    if oper == "1":
         print("Resultado:", soma(num1, num2))
    elif oper == "2":
         print("Resultado:", diferenca(num1, num2))
    elif oper == "3":
         print("Resultado:", multiplicacao(num1, num2))
    elif oper == "4":
         print("Resultado:", divisao(num1, num2))
    else:
         print("Opção inválida")

calculadora()

limpa_tela()

# ====================================================
# Exercicio 3:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.






# ====================================================
# Exercicio 4:
# Reescreva o exercício da quitanda do capítulo 2 separando as operações
# em funções, e adiconando um preço para cada fruta, de modo que quando a pessoa
# fizer o checkout ela consiga ver o preço total a pagar.




