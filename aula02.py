# =========================================
nome = "flavio custodio"  #string
idade = 27                #integer
altura = 1.79             #Float
acordado = True           #Boolean

numero1 = 10
numero2 = 25

numero3 = numero1 + numero2
print(numero3) 

frase = "O dia esta ensolarado"
frase2 = 'Vou aproveitar "bastante" e passear'
frase3 = """Eu também tenho esta opção 
para display de multiplas linhas
bem interessante isso """

print (frase)
print (frase2)
print (frase3)

# Operadores aritméticos
# + - * / 

# Operadores de comparação
# igualdade    ->   ==
# desigualdade ->   !=
print(numero1 == numero2)
print(numero1 != numero2)

# maior que   ->    >
# menor que   ->    <
print(numero1 > numero2)
print(numero1 < numero2)

# maior ou igual ->    >=
# menor ou igual ->    <=
print(numero1 > numero2)
print(numero1 < numero2)


# =========================================
nome = "flavio custodio"  #string
idade = 27                #integer
altura = 1.79             #Float
acordado = True           #Boolean

idade = str(idade)
idade = int(idade)
idade = float(idade)

# ===================================================
# Metodos de Strings

nome = "Flavio Custodio"
print(nome.replace("F","G")
)

# ==================================================
# Estruturas de Decisão

idade = int(input("Qual a sua idade? "))
if idade >= 18: 
    print("Você pode entrar.")
    print("Você tem mais de 18 anos")

elseif idade >=15:
    print("Você pode entrar, mas nao pode comprar bebida")

else:
    print("Você não pode entrar")

# ==================================================
# Estruturas de Repetição
# While - For

idade = 15
while idade <= 17:
    print("Você é muito novo para entrar.")
    print("Próximo!")
    idade = int(input("Qual a sua idade? ")) 


contador = 0
while contador < 10:
    print("Contando 10 vezes")
    contador = contador + 1

while True: 
    resposta = input("Você quer parar o programa? [S/N] ")
    resposta = resposta.upper()

    if resposta == "S":
        break

for numero in range (0, 10):
    print(numero)

for letra in "Flavio Custodio":
    print(letra)

for letra in range (0,15):
    print(letra)
