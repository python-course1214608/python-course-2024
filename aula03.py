# Coleções
# Tuplas, Listas, Dicionários, Sets
# =========================================
# Tuplas - São imutáveis

variavel1 = ("Arroz","Queijo","Leite","Beterraba")
# INDEX         0        1       2         3
print(variavel1)

variavel1 = ("Arroz","Queijo","Leite","Beterraba", 50, 75.3, False)
# INDEX         0        1       2         3        4    5     6
print(variavel1[3])

 variavel2 = "Arroz, Queijo, Leite, Beterraba"

for item in variavel1: 
    print(item)

# =========================================
# Listas - São mutáveis

variavel2 = ["Arroz","Queijo","Leite","Beterraba", 50, 75.3, False]
# INDEX         0        1       2         3        4    5     6

print(variavel2[3])

for item in variavel2:
    print(item)

# Exibe o index correspondente a solicitação
print(variable2.index("Leite"))
# Exibe a quantidade de strings encontrada na variavel
print(variable2.count("Beterraba"))

print(len(variavel2))

# Remove um valor dentro da variável passando o valor como argumento
variavel2.remove(False)
# Remove um valor pelo index - neste caso o index 2
variavel2.pop(2)
# Adicionar conteudo no final da lista
variavel2.append("Cachorro quente")
# Adicionar conteudo atraves de um index específico
variavel2.insert(2,"Faca de manteiga")

print(variavel2)


# =========================================
# Dicionários - Hash Tables

#           {"Chave": "valor"}
variavel3 = {"Idade": 13, "Cor": "vermelho", "animal": "gato"}
#           |-----------|  |---------------|  |--------------| 
#               Item              Item             Item

idade = 13
cor = "vermelho" 
animal = "gato"

print(variavel3["animal"])
print(variavel3["Cor"])

# Adicionar um item no dicionário com chave "nome" e valor "Tiago"
variavel3["nome"] = "Tiago"
# Remover um valor do dicionario com pop
variavel3.pop("idade")
# Remover um valor do dicionario com função DEL
del(variavel3["cor"])

print(variavel3)

for objeto in variavel3:
    print(objeto)

for objeto in variavel3.keys():
    print(objeto)

for objeto in variavel3.values()
    print(objeto)


# =========================================
# Sets: possui apenas as chaves na definição (sem os valores)
# Não permite valor repetido dentro de um set

variavel4 = {"idade","cachorro",12, False, 500}
print(variavel4)


variavel4 = ["idade","cachorro",12,False,500,"idade",12]
variavel4 = set(variavel4)      # convertendo em SET
variavel4 = list(variavel4)     # convertendo em Lista

print(variavel4)



































